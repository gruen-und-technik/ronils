#!/usr/bin/env python3

# from math import floor

# terminal interface currently deprecated. will be rewritten in future

class Terminal:

    def __init__(self, meeting):
        self.meeting = meeting
        self.commands = {
                'help'      : "help (h): display help",
                'list'      : "list (l): list enabled members",
                'enable'    : "enable (e): enable member",
                'disable'   : "disable (d): disable member",
                'backup'    : "backup (b): load member state from backup",
                'exit'      : "exit (x, q): Exit program"
                }
        # todo search if alias keys in dicts exist

    def help(self):
        for c in self.commands.values():
            print(c)

    def list(self):
        print("# Anwesende Personen")
        for f in self.meeting.enabled.keys():
            print(f"## {f} {len(self.meeting.enabled[f])}/?")
            for c in self.meeting.enabled[f]:
                print(f"* {c}")

    def members_state_change(self, newstate=True, persons=None):
        """
        todo copy desciption from gui
        """
        if not persons:
            faction =      input("Fraktion: ")
            name    =      input("Name:     ")
            votes   = int( input("Stimmen:  "))
        for p in persons:
            if newstate:
                self.meeting.arrive()
            else:
                self.meeting.leave()
        print(self.meeting.enabled)

    def members_arrive(self, persons=None):
        if not persons:
            print("# Enable a person")
        self.members_state_change(newstate=True, persons=persons)

    def members_leave(self, persons=None):
        if not persons:
            print("# Disable a person")
        self.members_state_change(newstate=False, persons=persons)

    def members_from_backup(self):
        self.meeting.arrive_from_backup()   # todo check path handling

    def show_speakers(self):
        print("Sperker list show not yet implemented")

    def run(self):
        running = True
        while running:
            try:
                command = input("°-°) ")
            except EOFError:
                command = "x"
            if command in ["help", "h", "?", "hilfe"]:
                self.help()
            elif command in ["list", "l"]:
                self.list()
            elif command in ["enable", "e"]:
                self.members_arrive()
            elif command in ["disable", "d"]:
                self.members_leave()
            elif command in ["backup", "b"]:
                self.members_from_backup()
            elif command in ["speakers", "s"]:
                self.show_speakers()
            # elif command in ["", ""]:
            #     self.()
            elif command in ["exit", "quit", "x", "q"]:
                running = False

if __name__ == "__main__":
    print("not runnable. import this file. use main.py with -t for terminal interface.")
